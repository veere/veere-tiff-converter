package br.com.veere.libs.tiff.result.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import br.com.veere.libs.tiff.TiffReader;
import br.com.veere.libs.tiff.TiffReader.TiffImageConsumer;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;

public class PdfResult {

	private TiffReader tiffReader;

	public PdfResult(TiffReader tiffReader) {
		this.tiffReader = tiffReader;
	}

	public void storedIn(OutputStream stream) throws IOException,
			DocumentException {
		final Document document = new Document(PageSize.LETTER, 0, 0, 0, 0);
		PdfWriter writer = PdfWriter.getInstance(document, stream);
		document.open();
		final PdfContentByte cb = writer.getDirectContent();
		tiffReader.read(new TiffImageConsumer() {

			@Override
			public void onImage(Image img) {
				document.setPageSize(new Rectangle(img.getScaledWidth(), img
						.getScaledHeight()));
				try {
					cb.addImage(img);
					document.newPage();
				} catch (DocumentException e) {
					throw new RuntimeException(e);
				}
			}
		});
		document.close();

	}

	public void storedIn(File file) throws IOException, DocumentException {
		final Document document = new Document(PageSize.LETTER, 0, 0, 0, 0);
		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(file));
		document.open();
		final PdfContentByte cb = writer.getDirectContent();
		tiffReader.read(new TiffImageConsumer() {

			@Override
			public void onImage(Image img) {
				document.setPageSize(new Rectangle(img.getScaledWidth(), img
						.getScaledHeight()));
				try {
					cb.addImage(img);
					document.newPage();
				} catch (DocumentException e) {
					throw new RuntimeException(e);
				}
			}
		});
		document.close();
	}

}
