package br.com.veere.libs.tiff;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.veere.libs.tiff.TiffConverter.TiffSource;

import com.lowagie.text.Image;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.TiffImage;

public class TiffReader {

	private TiffSource source;

	public TiffReader(TiffSource source) {
		this.source = source;
	}

	public void read(TiffImageConsumer consumer) throws IOException {
		RandomAccessFileOrArray tiff = new RandomAccessFileOrArray(
				source.read());
		int numberOfPages = TiffImage.getNumberOfPages(tiff);
		for (int p = 1; p <= numberOfPages; p++) {
			Image img = TiffImage.getTiffImage(tiff, p);
			img.scalePercent(7200f / img.getDpiX(), 7200f / img.getDpiY());
			img.setAbsolutePosition(0, 0);
			consumer.onImage(img);
		}
		tiff.close();

	}

	public List<Image> getImages() throws IOException {
		final List<Image> images = new ArrayList<Image>();
		read(new TiffImageConsumer() {
			public void onImage(Image image) {
				images.add(image);
			}
		});
		return images;
	}

	public interface TiffImageConsumer {
		void onImage(Image image);
	}
}
