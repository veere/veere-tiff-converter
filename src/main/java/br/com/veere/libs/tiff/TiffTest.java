/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.veere.libs.tiff;

import java.io.File;

/**
 *
 * @author veere-clecius
 */
public class TiffTest {

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			args = new String[] { "/home/veere-clecius/teste.tif" };
		}
		for (int i = 0; i < args.length; i++) {
			String tiff = args[i];
			String pdf = tiff.substring(0, tiff.lastIndexOf('.') + 1) + "pdf";
			try {
				new TiffConverter(tiff).toPdf().storedIn(new File(pdf));
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
}
