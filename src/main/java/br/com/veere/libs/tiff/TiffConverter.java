package br.com.veere.libs.tiff;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import br.com.veere.libs.tiff.result.pdf.PdfResult;

public class TiffConverter {

	private final TiffSource source;
	
	public TiffConverter(final byte[] file) {
		this.source = new TiffSource() {

			@Override
			public InputStream read() throws IOException {
				return new ByteArrayInputStream(file);
			}
		};
	}

	public TiffConverter(final String file) {
		this.source = new TiffSource() {

			@Override
			public InputStream read() throws IOException {
				return new FileInputStream(new File(file));
			}
		};
	}

	public TiffConverter(final File file) {
		this.source = new TiffSource() {

			@Override
			public InputStream read() throws IOException {
				return new FileInputStream(file);
			}
		};
	}

	public TiffConverter(final InputStream is) {
		this.source = new TiffSource() {

			@Override
			public InputStream read() throws IOException {
				return is;
			}
		};
	}
	
	public PdfResult toPdf(){
		return new PdfResult(new TiffReader(source));
	}

	public interface TiffSource {
		public InputStream read() throws IOException;
	}
}
